﻿using System;
using Xamarin.Forms;

namespace deleedtest
{
	public class PageRequest : ContentPage
	{
		Button logoutButton;

		public PageRequest()
		{
			this.Padding = new Thickness(20, Device.OnPlatform(40, 20, 20), 20, 20);

			StackLayout panel = new StackLayout
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Orientation = StackOrientation.Vertical,
				Spacing = 15,
			};

			panel.Children.Add(logoutButton = new Button
			{
				Text = "Logout"
			});

			panel.Children.Add(new Label
			{
				Text = "Logged In, you can create a request.. maybe.. ;)",
			});

			logoutButton.Clicked += logout;

			this.Content = panel;
		}

		private async void logout(object sender, EventArgs e)
		{
			// TODO to be replaced with social login with facebook feature
			if (await this.DisplayAlert(
					"User",
					"logout",
					"Yes",
					"No"))
			{
				// in case of logout
				App.Database.DeleteConfiguration("token");

				Navigation.InsertPageBefore(new PageLogin(), this);
				await Navigation.PopAsync().ConfigureAwait(false);

			}
		}
	}
}

