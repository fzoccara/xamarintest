﻿using System;
using Xamarin.Forms;

namespace deleedtest
{
	public class PageLogin : ContentPage
	{
		Button facebookLoginButton;
		Button twitterLoginButton;
		Button credentialsLoginButton;

		public PageLogin()
		{
			this.Padding = new Thickness(20, Device.OnPlatform(40, 20, 20), 20, 20);

			StackLayout panel = new StackLayout
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Orientation = StackOrientation.Vertical,
				Spacing = 15,
			};

			panel.Children.Add(facebookLoginButton = new Button
			{
				Text = "Facebook Login",
			});

			panel.Children.Add(twitterLoginButton = new Button
			{
				Text = "Twitter Login",
			});

			panel.Children.Add(credentialsLoginButton = new Button
			{
				Text = "Credentials Login",
				IsEnabled = false
			});

			facebookLoginButton.Clicked += facebookLogin;
			twitterLoginButton.Clicked += twitterLogin;
			credentialsLoginButton.Clicked += credentialLogin;

			this.Content = panel;
		}


		private async void facebookLogin(object sender, EventArgs e)
		{
			// TODO to be replaced with social login with facebook feature
			if (await this.DisplayAlert(
					"Facebook",
					"login with",
					"Yes",
					"No"))
			{
				// in case facebook login works

				var Configuration = new Configuration();
				Configuration.key = "token";
				Configuration.value = "fb-test";
				App.Database.SaveConfiguration(Configuration);

				Navigation.InsertPageBefore(new PageRequest(), this);
				await Navigation.PopAsync().ConfigureAwait(false);

			}
		}

		private async void twitterLogin(object sender, EventArgs e)
		{
			// TODO to be replaced with social login with twitter feature
			if (await this.DisplayAlert(
					"Twitter",
					"login with",
					"Yes",
					"No"))
			{
				// in case twitter login works

				var Configuration = new Configuration();
				Configuration.key = "token";
				Configuration.value = "tw-test";
				App.Database.SaveConfiguration(Configuration);

				Navigation.InsertPageBefore(new PageRequest(), this);
				await Navigation.PopAsync().ConfigureAwait(false);

			}
		}

		private async void credentialLogin(object sender, EventArgs e)
		{
			// TODO to be replaced with credential login feature
			if (await this.DisplayAlert(
					"Credentials",
					"login with",
					"Yes",
					"No"))
			{
				// in case credential login works

				var Configuration = new Configuration();
				Configuration.key = "token";
				Configuration.value = "cr-test";
				App.Database.SaveConfiguration(Configuration);

				Navigation.InsertPageBefore(new PageRequest(), this);
				await Navigation.PopAsync().ConfigureAwait(false);

			}
		}
	}
}

