﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SQLite;

namespace deleedtest
{
	public class DeleedDatabase 
	{
		static object locker = new object ();

		SQLiteConnection database;

		string DatabasePath {
			get { 
				var sqliteFilename = "deleed.db3";
				#if __IOS__
				string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
				string libraryPath = Path.Combine (documentsPath, "..", "Library"); // Library folder
				var path = Path.Combine(libraryPath, sqliteFilename);
				#else
				#if __ANDROID__
				string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
				var path = Path.Combine(documentsPath, sqliteFilename);
				#else
				// WinPhone
				var path = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, sqliteFilename);;
				#endif
				#endif
				return path;
			}
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="deleedtest.DL.deleedDatabase"/> DeleedDatabase. 
		/// if the database doesn't exist, it will create the database and all the tables.
		/// </summary>
		/// <param name='path'>
		/// Path.
		/// </param>
		public DeleedDatabase()
		{
			database = new SQLiteConnection (DatabasePath);
			// create the tables
			database.CreateTable<Configuration>();
		}

		public IEnumerable<Configuration> GetConfigurations ()
		{
			lock (locker) {
				return (from i in database.Table<Configuration>() select i).ToList();
			}
		}

		public Configuration GetConfiguration (string key) 
		{
			lock (locker) {
				return database.Table<Configuration>().FirstOrDefault(x => x.key == key);
			}
		}

		public int SaveConfiguration (Configuration item) 
		{
			lock (locker) {
				if (item.id != 0)
				{
					database.Update(item);
					return item.id;
				} else 
				{
					return database.Insert(item);
				}
			}
		}

		public int DeleteConfiguration(string key)
		{
			lock (locker) {
				Configuration item = GetConfiguration(key);
				return database.Delete<Configuration>(item.id);
			}
		}
	}
}

