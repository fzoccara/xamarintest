﻿using System;
using Xamarin.Forms;

namespace deleedtest
{
	public class App : Application
	{
		static DeleedDatabase database;
		//Customer customer;
		Configuration token = null;

		public static DeleedDatabase Database
		{
			get
			{
				database = database ?? new DeleedDatabase();
				return database;
			}
		}

		public App()
		{
			NavigationPage nav;

			loadResources();

			if ( token!= null && token.id != 0)
			{
				nav = new NavigationPage(new PageRequest()) { Title = "Create Request" };
			}
			else
			{
				// TODO to be replaced with a popup page type to avoid page navigation for login page
				nav = new NavigationPage(new PageLogin()) { Title = "Please Login" };
			}

			this.MainPage = nav;
		}

		private void loadResources()
		{
			// TODO to be replaced with more complex auth token and refresh token checks
			token = App.Database.GetConfiguration("token");
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}
