﻿using System;
using System.Collections.Generic;
using System.Linq;
using deleedtest.Models;
using SQLite.Net;
using SQLite.Net.Interop;
using SQLite.Net.Async;
using System.Threading.Tasks;

namespace deleedtest
{
	public class PreferenceRepository
	{
		private SQLiteAsyncConnection dbConn;

		public string StatusMessage { get; set; }

		public PreferenceRepository(ISQLitePlatform sqlitePlatform, string dbPath)
		{
			//initialize a new SQLiteConnection 
			if (dbConn == null)
			{
				var connectionFunc = new Func<SQLiteConnectionWithLock>(() =>
					new SQLiteConnectionWithLock
					(
						sqlitePlatform,
						new SQLiteConnectionString(dbPath, storeDateTimeAsTicks: false)
					));

				dbConn = new SQLiteAsyncConnection(connectionFunc);
				dbConn.CreateTableAsync<Preference>();
			}
		}

		public async Task AddNewPreferenceAsync(string name)
		{
			int result = 0;
			try
			{
				//basic validation to ensure a name was entered
				if (string.IsNullOrEmpty(name))
					throw new Exception("Valid name required");

				//insert a new preference into the Preference table
				result = await dbConn.InsertAsync(new Preference { Name = name });
				StatusMessage = string.Format("{0} record(s) added [Name: {1})", result, name);
			}
			catch (Exception ex)
			{
				StatusMessage = string.Format("Failed to add {0}. Error: {1}", name, ex.Message);
			}

		}

		public async Task<List<Preference>> GetAllPreferenceAsync()
		{
			//return a list of preference saved to the Preference table in the database
			List<Preference> preference = await dbConn.Table<Preference>().ToListAsync();
			return preference;
		}
	}
}