﻿using System;

using Xamarin.Forms;

namespace deleedtest
{
	public class PageLogin : ContentPage
	{
		Button facebookLoginButton;
		Button twitterLoginButton;
		Button credentialsLoginButton;

		public PageLogin()
		{

			this.Padding = new Thickness(20, Device.OnPlatform(40, 20, 20), 20, 20);

			StackLayout panel = new StackLayout
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Orientation = StackOrientation.Vertical,
				Spacing = 15,
			};

			panel.Children.Add(facebookLoginButton = new Button
			{
				Text = "Facebook Login",
			});

			panel.Children.Add(twitterLoginButton = new Button
			{
				Text = "Twitter Login",
			});

			panel.Children.Add(credentialsLoginButton = new Button
			{
				Text = "Credentials Login",
				IsEnabled = false
			});

			facebookLoginButton.Clicked += facebookLogin;
			twitterLoginButton.Clicked += twitterLogin;
			credentialsLoginButton.Clicked += credentialLogin;

			this.Content = panel;
		}

		private void facebookLogin()
		{
		}

		private void twitterLogin()
		{
		}

		private void credentialLogin()
		{
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}

