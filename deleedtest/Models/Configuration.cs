﻿using System;
using SQLite;

namespace deleedtest
{
	//[Table("configuration")]
	public class Configuration
	{
		public Configuration()
		{
		}

		[PrimaryKey, AutoIncrement, Column("ID")]
		public int id { get; set; }

		[Indexed, MaxLength(250), Column("Configuration Key")]
		public string key { get; set; }

		[MaxLength(250), Column("Configuration Value")]
		public string value { get; set; }
		
		/*[MaxLength(250), Column("Token")]
		public string token { get; set; }

		[MaxLength(250), Column("Refresh Token")]
		public string refresh_token { get; set; }

		[MaxLength(250), Column("Last Page")]
		public string lastpage { get; set; }

		[MaxLength(250), Column("Position")]
		public string lastpage { get; set; }*/
	}
}
